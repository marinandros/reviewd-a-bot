import config from 'config'

export interface IServerConfiguration {
  port: number
}

export function getServerConfig (): IServerConfiguration {
  return config.get('server')
}